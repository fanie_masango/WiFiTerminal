package com.example.wifiterminal;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainActivity extends AppCompatActivity implements TCPEvent {

    BroadcastReceiver broadcastReceiver;

    int cn = 0;
    TCPConnect.Client client;
    TCPConnect.Server server;
    boolean clientSelected = false;
    String serverIP;
    int serverPort;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        server = new TCPConnect.Server(11011);
        server.addListener(this);

        client = new TCPConnect.Client();
        client.addListener(this);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        savePreferences();
    }

    private void savePreferences(){
        //SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        //SharedPreferences sharedPref = getSharedPreferences("mySettings", MODE_PRIVATE);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();

        editor.putString("clientIP",server.SocAddress);
        editor.putInt("serverPort",serverPort);
        editor.putString("serverIP", serverIP);
        editor.putInt("serverPort",serverPort);
        editor.commit();
    }

    public void connectToServer(View view){

        EditText portEt = findViewById(R.id.portEditText);
        EditText ipEt = findViewById(R.id.ipAddressEditText);

         serverIP = String.valueOf(ipEt.getText());
         serverPort = Integer.parseInt(String.valueOf(portEt.getText()));

        System.out.println(serverIP);
        System.out.println(serverPort);
        client.StartConnection(serverIP,serverPort);
    }

    public void handleTransmissions(){
        cn++;
        //   TextView tv1 = findViewById(R.id.ipAddressTextView);
        //    tv1.setText(Integer.toString(cn));
    }

    @Override
    protected void onStart(){
        super.onStart();
        broadcastReceiver = new BroadcastReceiver(){
            @Override
            public void onReceive(Context ctx, Intent intent){
                if(intent.getAction().compareTo(Intent.ACTION_TIME_TICK) == 0) {
                    handleTransmissions();
                }
            }
        };
        registerReceiver(broadcastReceiver, new IntentFilter(Intent.ACTION_TIME_TICK));
    }

    public void ClientSelection(View view){
        clientSelected = true;
        CardView clientView = findViewById(R.id.cardViewClient);
        CardView serverView = findViewById(R.id.cardViewServer);
        clientView.setCardBackgroundColor(getResources().getColor(R.color.white));
        serverView.setCardBackgroundColor(getResources().getColor(R.color.selected_color));
    }
    public void ServerSelection(View view){
        clientSelected = false;
        CardView clientView = findViewById(R.id.cardViewClient);
        CardView serverView = findViewById(R.id.cardViewServer);
        clientView.setCardBackgroundColor(getResources().getColor(R.color.selected_color));
        serverView.setCardBackgroundColor(getResources().getColor(R.color.white));
    }

    public void checkIP(View view) {
        TextView tv1 = findViewById(R.id.ipAddressTextView);
        tv1.setText("TEST:" + server.SocAddress);
        TextView tv2 = findViewById(R.id.portTextView);
        tv2.setText(Integer.toString(server.port));
    }

    public void sendMessage(View view){
        EditText edtxt = findViewById(R.id.editTextTxMessage);
        String text = String.valueOf(edtxt.getText())+'\n';
        EditText et = findViewById(R.id.RxTxt);
        edtxt.setText("");
        int spanColor;

        Spannable spannable = new SpannableString(text);

        if(client.getConnectionState() && !server.getConnectionState()) {
            client.sendMessage(text);
            spanColor = Color.GREEN;
        }else if(!client.getConnectionState() && server.getConnectionState()) {
            server.sendMessage(text);
            spanColor = Color.GREEN;
        }else if(!client.getConnectionState() && !server.getConnectionState()) {
             text = "client/server not connected";
            spanColor = Color.RED;
        }else {
            spanColor = Color.GREEN;
            if(clientSelected)
                client.sendMessage(text);
            else
                server.sendMessage(text);
        }
                spannable.setSpan(new ForegroundColorSpan(spanColor), 0, text.length(),
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                et.append(spannable);
    }

    @Override
    public void dataAvailable(int connectionstate, String data) {

        System.out.println("data:"+data);
        System.out.println(connectionstate);
        TextView iptxt = findViewById(R.id.ipAddressTextView);
        TextView porttxt = findViewById(R.id.portTextView);
        EditText et = findViewById(R.id.RxTxt);
        EditText clientConTxt = findViewById(R.id.editTextClientInfo);
        EditText serverConTxt = findViewById(R.id.editTextServerInfo);
        Resources res = this.getResources();
        final ImageView image = (ImageView) findViewById(R.id.ServerStatusIndicator);
        final int ncColor = res.getColor(R.color.server_no_op);
        final int swColor = res.getColor(R.color.server_waiting);
        final int scColor = res.getColor(R.color.server_connected);

        switch (connectionstate)
        {
            case TCPConnect.SERVER_STATE_NO_CONNECTION:
                image.setColorFilter(ncColor, PorterDuff.Mode.SRC_ATOP);
                runOnUiThread(() -> {
                    iptxt.setText(server.SocAddress);
                    porttxt.setText(Integer.toString(server.port));
                    serverConTxt.append("-> "+data+'\n');
                });
                break;
            case TCPConnect.SERVER_STATE_WAITING:
                savePreferences();
                image.setColorFilter(swColor, PorterDuff.Mode.SRC_ATOP);
                runOnUiThread(() -> {
                    iptxt.setText(server.SocAddress);
                    porttxt.setText(Integer.toString(server.port));
                    serverConTxt.append("-> "+data+'\n');
                });
                break;
            case TCPConnect.SERVER_STATE_CONNECTED:
                image.setColorFilter(scColor, PorterDuff.Mode.SRC_ATOP);
                runOnUiThread(() -> {
                    iptxt.setText(server.SocAddress);
                    porttxt.setText(Integer.toString(server.port));
                    serverConTxt.append("-> "+data+'\n');
                });
                break;
            case TCPConnect.CLIENT_STATE_NO_CONNECTION:
                runOnUiThread(() -> {clientConTxt.append(data + '\n');});
                break;
            case TCPConnect.SERVER_STATE_MESSAGE_RECV:
                runOnUiThread(() -> {
                    iptxt.setText(server.SocAddress);
                    porttxt.setText(Integer.toString(server.port));
                    et.append("-> "+data+'\n');
                });
                break;
            case TCPConnect.CLIENT_STATE_MESSAGE_RECV:
                runOnUiThread(() -> {et.append("-> "+data+'\n');});
                break;
        }
    }
}




