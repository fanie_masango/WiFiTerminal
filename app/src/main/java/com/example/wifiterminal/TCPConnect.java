package com.example.wifiterminal;

import android.content.ComponentName;
import android.content.Intent;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;


// An interface to be implemented by everyone interested in "Hello" events
interface TCPEvent {
    void dataAvailable(int connectionstate, String data);
}


public interface TCPConnect {
    int SERVER_STATE_NO_CONNECTION = 600;
    int SERVER_STATE_WAITING = 601;
    int SERVER_STATE_CONNECTED = 602;
    int SERVER_STATE_MESSAGE_RECV = 603;
    int CLIENT_STATE_NO_CONNECTION = 800;
    int CLIENT_STATE_WAITING = 801;
    int CLIENT_STATE_CONNECTED = 802;
    int CLIENT_STATE_MESSAGE_RECV = 803;

    //--------------------Client class-------------------------//

    class Client implements Runnable{
        private Thread thread;
        private int port;
        private String IPAddress;
        public int clientState = CLIENT_STATE_NO_CONNECTION;

        boolean readyToTransmit = false;
        String TxMessage = "";

        private Socket  clientSocket;
        private PrintWriter out;
        private BufferedReader in;
        private boolean connected = false;

        public boolean getConnectionState(){
            return connected;
        }

        DataInputStream dataInputStream;
        DataOutputStream dataOutputStream;
        BufferedInputStream bufferedInputStream;
        BufferedOutputStream bufferedOutputStream;
        private boolean connectToServer = false;

        private List<TCPEvent> listeners = new ArrayList<>();
        public void addListener(TCPEvent _listener){
            listeners.add(_listener);
        }
        private void broadcastEvent(int clientstate, String message){
            for (TCPEvent hl : listeners)
                hl.dataAvailable(clientstate, message);
        }

        public Client(){
            this.thread = new Thread(this);
            this.thread.setPriority(Thread.NORM_PRIORITY);

        }
        public void sendMessage(String message){
            readyToTransmit = true;
            TxMessage = message;
        }
        public void StartConnection(String ip, int port){
            this.port = port;
            this.IPAddress = ip;
            clientState = CLIENT_STATE_NO_CONNECTION;
            connectToServer = true;
            if(!thread.isAlive())
                this.thread.start();
        }
        @Override
        public void run() {
            InputStream inStream = null;
            while (true){
                if(connectToServer == true){
                    try {
                        clientSocket = new Socket(IPAddress,port);
                        broadcastEvent(clientState, "connected");
                        connected = true;
                    } catch (IOException e) {
                        connected = false;
                        broadcastEvent(clientState, e.getMessage());
                    }
                    connectToServer = false;
                }
                if(clientSocket == null) {

                }
                else {
                    try {

                        try {
                            this.dataInputStream = new DataInputStream( new BufferedInputStream( this.
                                    clientSocket.getInputStream() ) );
                            this.dataOutputStream = new DataOutputStream( new BufferedOutputStream( this.
                                    clientSocket.getOutputStream() ) );
                        }catch (IOException e){
                            broadcastEvent(clientState,e.getMessage());
                        }

                        if(dataInputStream.available() > 0)
                        {
                            clientState = CLIENT_STATE_MESSAGE_RECV;
                            connected = true;
                            BufferedReader reader = new BufferedReader(new InputStreamReader(dataInputStream));
                            String line = reader.readLine();    // reads a line of text
                            broadcastEvent(clientState,line);
                            System.out.println(line);
                        }
                    } catch (IOException e) {
                        System.out.println(e.getStackTrace());
                        broadcastEvent(clientState,e.getMessage());
                    }

                    if(readyToTransmit){
                        // send welcome message
                        try {
                            this.dataOutputStream.writeBytes(TxMessage);
                            this.dataOutputStream.flush();
                        } catch ( IOException e )
                        {
                            broadcastEvent(clientState,e.getMessage());
                        }
                        //-------------------
                        readyToTransmit = false;
                    }
                }



                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }

        }
    }
    //-------------------------------------------------------------//

    public class Server implements Runnable {
        private String TxMessage;
        private boolean readyToTransmit = false;
        int SERVER_HEARTBEAT_TIMER = 50000;
        private int serverstatus = 0;
        private String RxMessage;
        public int getServerStatus(){
            return serverstatus;
        }
        public String getRxMessage(){
            return RxMessage;
        }

        private List<TCPEvent> listeners = new ArrayList<>();
        public void addListener(TCPEvent _listener){
            listeners.add(_listener);
        }

        private void triggerBroadCast(String firstFavApp, String secondFavApp) {
            Intent intent = new Intent("FavAppsUpdated");
            intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
            intent.putExtra("FIRST_FAV_APP", firstFavApp);
            intent.putExtra("SECOND_FAV_APP", secondFavApp);
            intent.setComponent(new
                    ComponentName("com.android.systemui",
                    "com.android.systemui.statusbar.phone.FavAppsChanged"));

        }


        private Thread thread;
        DataInputStream dataInputStream;
        DataOutputStream dataOutputStream;
        BufferedInputStream bufferedInputStream;
        BufferedOutputStream bufferedOutputStream;
        private Socket socket = null;
        private ServerSocket serverSocket = null;
        int port;
        int cnt;
        String SocAddress;
        private boolean connected = false;

        public static String getIPAddress(boolean useIPv4) {
            try {
                List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
                for (NetworkInterface intf : interfaces) {
                    List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                    for (InetAddress addr : addrs) {
                        if (!addr.isLoopbackAddress()) {
                            String sAddr = addr.getHostAddress();
                            //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                            boolean isIPv4 = sAddr.indexOf(':')<0;

                            if (useIPv4) {
                                if (isIPv4)
                                    return sAddr;
                            } else {
                                if (!isIPv4) {
                                    int delim = sAddr.indexOf('%'); // drop ip6 zone suffix
                                    return delim<0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toUpperCase();
                                }
                            }
                        }
                    }
                }
            } catch (Exception ex) { } // for now eat exceptions
            return "";
        }

        public boolean getConnectionState(){
            return connected;
        }
        public Server(int port) {
            this.port = port;
            this.thread = new Thread(this);
            this.thread.setPriority(Thread.NORM_PRIORITY);
            this.thread.start();
            cnt = 0;
        }
        private void broadcastEvent(int serverstate, String message){
            for (TCPEvent hl : listeners)
                hl.dataAvailable(serverstate, message);
        }

        public void sendMessage(String message){
            readyToTransmit = true;
            TxMessage = message;
        }

        @Override
        public void run() {
            int heartbeat = 0;
            serverstatus = SERVER_STATE_NO_CONNECTION; ///
            connected = false;
            while (true){
                //create socket
                try {
                    broadcastEvent(serverstatus,"creating server...");
                    this.serverSocket = new ServerSocket(port);
                }catch (IOException e){
                    broadcastEvent(serverstatus,e.getMessage());
                }
                SocAddress = getIPAddress(true);

                try {
                    serverstatus = SERVER_STATE_WAITING;
                    connected = false;
                    broadcastEvent(serverstatus,"server running...");
                    socket = serverSocket.accept();

                }catch (IOException e){
                    broadcastEvent(serverstatus,e.getMessage());
                }
                serverstatus = SERVER_STATE_CONNECTED;
                connected = true;
                broadcastEvent(serverstatus,"connected");
                try {
                    this.dataInputStream = new DataInputStream( new BufferedInputStream( this.
                            socket.getInputStream() ) );
                    this.dataOutputStream = new DataOutputStream( new BufferedOutputStream( this.
                            socket.getOutputStream() ) );
                }catch (IOException e){
                    broadcastEvent(serverstatus,e.getMessage());
                }

                // send welcome message
                try {
                    this.dataOutputStream.writeBytes("hello from server...");
                    this.dataOutputStream.flush();
                } catch ( IOException e )
                {
                    broadcastEvent(serverstatus,e.getMessage());
                }
                // placeholder recv loop
                while ( true )
                {
                    InputStream in = null;
                    try {
                        in = socket.getInputStream();

                        if(in.available() > 0)
                        {
                            serverstatus = SERVER_STATE_MESSAGE_RECV;
                            connected = true;
                            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                            String line = reader.readLine();    // reads a line of text
                            broadcastEvent(serverstatus,line);
                        }
                    } catch (IOException e) {
                        System.out.println(e.getStackTrace());
                        broadcastEvent(serverstatus,e.getMessage());
                    }
                    if(readyToTransmit == true){
                        try {
                            this.dataOutputStream.writeBytes(TxMessage);
                            this.dataOutputStream.flush();
                            readyToTransmit = false;
                        } catch ( IOException e )
                        {
                            try {
                                dataInputStream.close();
                                serverSocket.close();
                                broadcastEvent(serverstatus,"Connection closed");
                                serverstatus = SERVER_STATE_NO_CONNECTION;
                                connected = false;
                            } catch (IOException ex) {
                                broadcastEvent(serverstatus,ex.getMessage());
                            }
                            connected = false;
                            break;
                        }
                    }

                    try {
                        Thread.sleep(1);
                        heartbeat++;
                    } catch (InterruptedException e) {
                        broadcastEvent(serverstatus,e.getMessage());
                    }
                    if(heartbeat == SERVER_HEARTBEAT_TIMER)
                    {
                        try {
                            this.dataOutputStream.writeChars("0");
                            this.dataOutputStream.flush();
                        } catch ( IOException e )
                        {
                            try {
                                dataInputStream.close();
                                serverSocket.close();
                                broadcastEvent(serverstatus,"Connection closed");
                                serverstatus = SERVER_STATE_NO_CONNECTION;
                            } catch (IOException ex) {
                                broadcastEvent(serverstatus,ex.getMessage());
                            }
                            connected = false;
                            break;
                        }
                        heartbeat = 0;
                    }
                }
            }

        }
    }
}